﻿using PaPaPa.Data;
using PaPaPa.Web.Business.Datings;
using PaPaPa.Web.Models.Datings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace PaPaPa.Areas.Dating.Controllers
{
    public class DatingInfoController : Controller
    {
        //
        // GET: /Dating/DatingInfo/
        public ActionResult Create(string message)
        {
            ViewBag.StatusMessage = message;
            return View();
        }

        //
        // POST: /Dating/DatingInfo/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(DatingInfoViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    model = await DatingInfoBusiness.CreateAsync(model, Int32.Parse(User.Identity.GetUserId()));
                    return RedirectToAction("Create", new { Message = "你的约会信息已创建。" });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            // 如果我们进行到这一步时某个地方出错，则重新显示表单
            return View(model);
        }


        public async Task<ActionResult> Modify(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var model = await DatingInfoBusiness.FindByIdAsync(id);
                    return View(model);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            // 如果我们进行到这一步时某个地方出错，则重新显示表单
            return RedirectToRoute(UrlConstant.HOME);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Modify(DatingInfoViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await DatingInfoBusiness.ModifyAsync(model, Int32.Parse(User.Identity.GetUserId()));
                    return RedirectToAction("Modify", new { Message = "你的约会信息已更改。" });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            // 如果我们进行到这一步时某个地方出错，则重新显示表单
            return View(model);
        }

        public async Task<ActionResult> List()
        {
            if (User.Identity.IsAuthenticated)
                return View(await DatingInfoBusiness.FindListAsync(Int32.Parse(User.Identity.GetUserId())));
            else
                return View();
        }
    }
}