﻿using Framework.Common.ExceptionOperation;
using Framework.Mapping.Base;
using PaPaPa.Core.Datings;
using PaPaPa.Models.Datings;
using PaPaPa.Web.Models.Datings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Web.Business.Datings
{
    /// <summary>
    /// 约会信息业务
    /// </summary>
    public class DatingInfoBusiness
    {
        /// <summary>
        /// 创建约会信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async static Task<DatingInfoViewModel> CreateAsync(DatingInfoViewModel model, int userId)
        {
            var mapper = new MapperBase<DatingInfoViewModel, DatingInfo>();
            var entity = mapper.GetEntity(model);
            entity.CreateUserId = userId;

            await DatingInfoCore.Create(entity);

            return mapper.GetModel(entity);
        }

        /// <summary>
        /// 修改约会信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async static Task<DatingInfoViewModel> ModifyAsync(DatingInfoViewModel model, int userId)
        {
            var mapper = new MapperBase<DatingInfoViewModel, DatingInfo>();

            var oldEntity = await DatingInfoCore.FindById(model.Id);

            if (oldEntity == null)
            {
                throw new GException("约会信息不存在");
            }

            var entity = mapper.GetEntity(model, oldEntity);
            entity.UpdateUserId = userId;

            await DatingInfoCore.Modify(entity);

            return mapper.GetModel(entity);
        }

        /// <summary>
        /// 通过Id查询约会信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async static Task<DatingInfoViewModel> FindByIdAsync(int id)
        {
            var entity = await DatingInfoCore.FindById(id);
            var mapper = new MapperBase<DatingInfoViewModel, DatingInfo>();
            return mapper.GetModel(entity);
        }

        /// <summary>
        /// 通过用户Id查询约会列表
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async static Task<List<DatingInfoViewModel>> FindListAsync(int userId)
        {
            var lstEntity = await DatingInfoCore.FindListByCondition(x => x.CreateUserId == userId);

            var mapper = new MapperBase<DatingInfoViewModel, DatingInfo>();
            return mapper.GetModelList(lstEntity);
        }
    }
}
